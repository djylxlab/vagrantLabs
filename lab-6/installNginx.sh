#!/bin/bash
echo '==== Mise à jour ===='
sudo apt update -y
echo '==== Dépendances ===='
sudo apt install epel-release -y 
sudo apt install nginx -y
echo '==== Lancement de Nginx ====='
sudo systemctl start nginx && sudo systemctl enable nginx
sudo systemctl status nginx