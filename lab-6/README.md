# VagrantLabs --> Vagrant plugin

Bienvenue sur le Repository Vagrant du Lab 6

installation du lab avec la box vagrantup de ubuntu/xenial64
(version minimaliste avec l'option -m)

Préciser le CPU (1), la ram (1Gio) et l'adresse IP souhaitée en variable pour les 3 machines.
(voir fichier Vagrantfile)

Réaliser un script afin que lors de l'installation de la VM nginx s'installe également sur les 2 serveurs web seulement.
(voir fichier Vagrantfile)

Il est nécessaire également de mettre en place le plugin vagrant-hostsupdater afin que le fichier hosts de chaque machine corresponde à la réalité de leur nom et leur ip.

pour lancer la construction de la box : vagrant up --provision
