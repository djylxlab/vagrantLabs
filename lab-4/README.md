# VagrantLabs --> Déploiement d'un serveur web
Bienvenue sur le Repository Vagrant du Lab 4

installation du lab avec la box vagrantup de geerlingguy/centos7
(version minimaliste avec l'option -m)

Préciser le CPU (2), la ram (2Gio) et l'adresse IP souhaitée en variable
(voir fichier Vagrantfile)

Réaliser un script afin que lors de l'installation de la VM, nginx s'installe également.
(voir fichier Vagrantfile)

pour lancer la construction de la box : vagrant up --provision
