# Vagrant

Bienvenue sur le Repository Vagrant !

# Description des Commandes de bases.

`Vagrant init` :

initialise le fichier "Vagrantfile". Attention de bien mettre en paramètre l'os et la version souhaité

`Vagrant validate` :

Valide le fichier précédemment créé

`Vagrant up` :

Permet de monter la machine virtuelle avec l'image souhaitée (mise en paramètre lors du "vagrant init")

`Vagrant status` :

Permet de voir le staus de la VM (si elle est en route ou pas)

`Vagrant global-status` :

Permet de voir l'état de toutes les "box" en route.

`Vagrant ssh` :

Nous permet de nous connecter à notre VM en ssh.

`Vagrant halt` :

Arrête la VM concernée

`Vagrant destroy` :

Supprime la VM

`Vagrant add box ubuntu/trusty64` :

Ne créé pas la VM mais prépare un environnment sous ubuntu en version trusty64 en allant récupéré la version souhaité sur le "vagrant Cloud" en l'installant dans un dossier C:\Users\NomUtilisateur\.vagrand.d\boxes

`Vagrant init -m ubuntu/trusty64` :

initie le "Vagrantfile" en précisant l'Os et la version choisie en version minimaliste (option -m).

# Objectif

Dans ce Repository, vous trouverez mes travaux sous Vagrant lors de ma Formation d'Administrateur Système Devops.

Chaque "Lab" est disposé dans un dossier spécifique.

Chaque "Lab" apporte une fonctionnalité différente par rapport au précédent qui est défini dans leur propre "README.md"