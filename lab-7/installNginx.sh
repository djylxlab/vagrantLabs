#!/bin/bash
echo '==== Mise à jour ===='
sudo apt update -y

echo '==== Dépendances ===='
sudo apt install nginx -y

echo '==== Vérification du status de Nginx ====='
sudo /etc/init.d/nginx status