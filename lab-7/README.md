# VagrantLabs --> WebApp Folder

Bienvenue sur le Repository Vagrant du Lab 7

installation du lab avec la box vagrantup de ubuntu/xenial64
(version minimaliste avec l'option -m)

Préciser le CPU (1), la ram (1Gio) et l'adresse IP souhaitée en variable.
(voir fichier Vagrantfile)

Réaliser un script afin que lors de l'installation de la VM il affiche le status de nginx.
(voir fichier Vagrantfile)

Après avoir télécharger le dossier du site web, faire en sorte de monter l'application.
(synced_folder)

Pour lancer la construction de la box : vagrant up
