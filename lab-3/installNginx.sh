#!/bin/bash
echo '==== Mise à jour ===='
sudo yum update -y
echo '==== Dépendances ===='
sudo yum install epel-release -y 
sudo yum install nginx -y
echo '==== Lancement de Nginx ====='
sudo systemctl start nginx && sudo systemctl enable nginx
sudo systemctl status nginx