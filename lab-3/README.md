# Vagrant --> Création d'un Vagrantfile

Bienvenue sur le Repository Vagrant du Lab 3

installation du lab avec la box vagrant de geerlingguy/centos7
(version minimaliste avec l'option -m)

Préciser le CPU (2) et la ram (2Gio) souhaitée en variable
(voir fichier Vagrantfile)

Réaliser un script afin que lors de l'installation de la VM nginx s'installe également.
(voir fichier Vagrantfile)

pour lancer la construction de la box : vagrant up --provision