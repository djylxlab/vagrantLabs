# VagrantLabs --> Installation de nginx à l'aide d'ansible

Bienvenue sur le Repository Vagrant du Lab 7

installation du lab avec la box vagrant de ubuntu/xenial64
(version minimaliste avec l'option -m)

Préciser le CPU (1), la ram (1Gio) et l'adresse IP souhaitée en variable pour votre machine.
(voir fichier Vagrantfile)

Avec "ansible_local" et un playbook nommé nginx.yml, installer nginx et le démarrer.
(voir fichier nginx.yml)

Pour lancer la construction de la box : vagrant up 
