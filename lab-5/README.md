# VagrantLabs --> Déploiement d'un serveur web

Bienvenue sur le Repository Vagrant du Lab 5

installation du lab avec la box vagrantup de ubuntu/xenial64
(version minimaliste avec l'option -m)

Préciser le CPU (1), la ram (1Gio) et l'adresse IP souhaitée en variable pour les 3 machines.
(voir fichier Vagrantfile)

Réaliser un script afin que lors de l'installation de la VM nginx s'installe également sur les 2 serveurs web seulement.
(voir fichier Vagrantfile)

pour lancer la construction de la box : vagrant up --provision
