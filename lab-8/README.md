# VagrantLabs --> Shell

Bienvenue sur le Repository Vagrant du Lab 8

installation du lab avec la box vagrant de ubuntu/xenial64
(version minimaliste avec l'option -m)

Préciser le CPU (1), la ram (1Gio) et l'adresse IP souhaitée en variable pour les 3 machines.
(voir fichier Vagrantfile)

Pour chacunes des VMs, un script bien bien spécifique doit être lancé lors de la création de celles-ci.
(cf scriptLb.sh, scriptWeb1.sh et scriptWeb2.sh)

Ces scripts permettent d'installer sur chacun d'eux un serveur Nginx. Sur "scriptLb.sh" Le serveur Nginx est paramétré comme reverse proxy afin de redirigérer le traffic sur les 2 autres serveurs.

Pour lancer la construction de la box : vagrant up --provision
