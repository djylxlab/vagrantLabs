#!/bin/bash

echo 'Starting Provision: lb1'
# Mise à jour du système
sudo apt-get update 

# Installation de nginx
sudo apt-get install -y nginx

# Arrêt du service Nginx
sudo service nginx stop

# Suppression et création du fichier "default"
sudo rm -rf /etc/nginx/sites-enabled/default
sudo touch /etc/nginx/sites-enabled/default

# Ce script permet de configurer le serveur Nginx en tant que Proxy inversé.
# Afin de rediriger le trafic vers les serveurs dont les adresses ip sont dans le groupe Testapp
echo "upstream testapp {
        server 10.0.0.11;
        server 10.0.0.12;
}

server {
    listen 80 default_server;
    listen [::]:80 default_server ipv6only=on;

    root /usr/share/nginx/html;
    index index.html index.htm;

    #Make site accessible from http://localhost/
    server_name localhost;

    location / {
            proxy_pass http://testapp;
    }
}" >> /etc/nginx/sites-enabled/default

# Lancement du service Nginx
sudo service nginx start
echo "Machine: lb1" > /var/www/index.html
echo 'Provision lb1 complete'